import Vue from "vue";
import Vuex from "vuex";

// import example from './module-example'

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state: {
      links: [
        {
          title: "Главная",
          url: "1",
          id: "1",
        },
        {
          title: "Фича",
          url: "2",
          id: "2",
        },
        {
          title: "Учителя",
          url: "3",
          id: "3",
        },
        {
          title: "Курсы",
          url: "4",
          id: "4",
        },
        {
          title: "О нас",
          url: "5",
          id: "5",
        },
        {
          title: "Контакты",
          url: "6",
          id: "6",
        },
        {
          title: "Филиалы",
          url: "7",
          id: "7",
        },
      ],
    },
    actions: {},
    mutations: {},
    getters: {
      links(state) {
        return state.links;
      },
    },
  });

  return Store;
}
